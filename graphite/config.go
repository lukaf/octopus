package graphite

import "os"

type Config struct {
	Endpoint string
	Username string
	Password string
}

func GetConfig() Config {
	return Config{
		Endpoint: GetSecret("GRAPHITE_ENDPOINT"),
		Username: GetSecret("GRAPHITE_USERNAME"),
		Password: GetSecret("GRAPHITE_PASSWORD"),
	}
}

func GetSecret(name string) string {
	return os.Getenv(name)
}
