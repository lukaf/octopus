package graphite

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"sort"
	"time"
)

type MetricType string

const (
	Gauge   MetricType = "gauge"
	Counter MetricType = "counter"
)

type Metric struct {
	Name     string     `json:"name"`
	Metric   string     `json:"metric"` // same as Name above
	Interval int        `json:"interval"`
	Value    float64    `json:"value"`
	Time     int64      `json:"time"`
	Mtype    MetricType `json:"mtype"`
}

type Metrics struct {
	Name     string
	Type     MetricType
	Interval int
	Metrics  []Metric
}

func (m *Metrics) Len() int {
	return len(m.Metrics)
}

func (m *Metrics) Less(i, j int) bool {
	return m.Metrics[i].Time > m.Metrics[j].Time
}

func (m *Metrics) Swap(i, j int) {
	m.Metrics[i], m.Metrics[j] = m.Metrics[j], m.Metrics[i]
}

func (m *Metrics) Add(value float64, timestamp time.Time) {
	m.Metrics = append(m.Metrics, Metric{
		Name:     m.Name,
		Metric:   m.Name,
		Interval: m.Interval,
		Value:    value,
		Time:     timestamp.Unix(),
		Mtype:    m.Type,
	})
}

func NewMetrics(name string, mtype MetricType, interval time.Duration) *Metrics {
	return &Metrics{
		Name:     name,
		Type:     mtype,
		Interval: int(interval.Seconds()),
		Metrics:  make([]Metric, 0),
	}
}

func Send(metrics *Metrics) error {
	client := &http.Client{}

	config := GetConfig()

	sort.Sort(metrics)

	data, err := json.Marshal(metrics.Metrics)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", config.Endpoint, bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(config.Username, config.Password)

	response, err := client.Do(req)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode > 299 {
		d, err := io.ReadAll(response.Body)
		if err != nil {
			return errors.New(response.Status)
		}
		return errors.New(
			fmt.Sprintf("%s: %s", response.Status, string(d)),
		)
	}

	return nil
}
