package graphite

import (
	"time"
)

func timeFormat(t time.Time) int64 {
	return t.Unix()
}
