package graphite

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

func TestSend(t *testing.T) {
	username := "username"
	password := "password"

	var authHeader string
	var body []byte

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader = r.Header.Get("Authorization")

		var err error
		body, err = ioutil.ReadAll(r.Body)
		if err != nil {
			t.Fatalf("error reading request body: %s", err)
		}

		fmt.Fprintln(w, "hello world")
	})

	ts := httptest.NewServer(handler)
	defer ts.Close()

	if err := os.Setenv("GRAPHITE_ENDPOINT", ts.URL); err != nil {
		t.Fatal("can't set GRAPHITE_ENDPOINT environment variable")
	}

	if err := os.Setenv("GRAPHITE_USERNAME", username); err != nil {
		t.Fatal("can't set GRAPHITE_USERNAME environment variable")
	}

	if err := os.Setenv("GRAPHITE_PASSWORD", password); err != nil {
		t.Fatal("can't set GRAPHITE_PASSWORD environment variable")
	}

	metricName := "testing.metric.egg"
	metricType := Gauge
	metricInterval := time.Second * 30
	metricValue := 0.123
	metricTimestamp := time.Now().UTC()

	metrics := NewMetrics(metricName, metricType, metricInterval)
	metrics.Add(metricValue, metricTimestamp)

	err := Send(metrics)
	if err != nil {
		t.Fatal(err)
	}

	expecedAuthHeader := fmt.Sprintf(
		"Basic %s",
		base64.StdEncoding.EncodeToString([]byte(username+":"+password)),
	)

	if authHeader != expecedAuthHeader {
		t.Fatalf("expected auth header '%s', got auth header '%s'\n", expecedAuthHeader, authHeader)
	}

	m := []Metric{}
	if err := json.Unmarshal(body, &m); err != nil {
		t.Fatalf("unable to decode JSON body: %s", err)
	}

	if len(m) != 1 {
		t.Fatalf("expected 1 metric, got %d\n", len(m))
	}

	metric := m[0]
	if metric.Name != metricName {
		t.Fatalf("expected metric name '%s', got '%s'\n", metricName, metric.Name)
	}

	if metric.Metric != metricName {
		t.Fatalf("expected metric metric '%s', got '%s'\n", metricName, metric.Metric)
	}

	if metric.Interval != int(metricInterval.Seconds()) {
		t.Fatalf("expected metric interval '%d', got '%d'\n", int(metricInterval.Seconds()), metric.Interval)
	}

	if metric.Value != metricValue {
		t.Fatalf("expected metric value '%f', got '%f'\n", metricValue, metric.Value)
	}

	if metric.Time != metricTimestamp.Unix() {
		t.Fatalf("expected metric time '%d', got '%d'\n", metricTimestamp.Unix(), metric.Time)
	}

	if metric.Mtype != metricType {
		t.Fatalf("expected metric type '%s', got '%s'\n", metricType, metric.Mtype)
	}
}

func TestMakeMetrics(t *testing.T) {
	mtype := Gauge
	interval := time.Minute * 30
	name := "testing.metric.egg"
	value := 0.123
	time := time.Now().UTC()

	metrics := NewMetrics(name, mtype, interval)
	metrics.Add(value, time)

	if len(metrics.Metrics) != 1 {
		t.Fatal("wrong number of metrics")
	}

	m := metrics.Metrics[0]

	if m.Name != name {
		t.Fatalf("wrong metric name, got '%s', want '%s'\n", m.Name, name)
	}

	if m.Metric != name {
		t.Fatalf("wrong metric metric, got '%s', want '%s'\n", m.Metric, name)
	}

	if m.Interval != int(interval.Seconds()) {
		t.Fatalf("wrong metric interval, got '%d', want '%d'\n", m.Interval, int(interval.Seconds()))
	}

	if m.Value != value {
		t.Fatalf("wrong metric value, got '%f', want '%f'\n", m.Value, value)
	}

	if m.Time != time.Unix() {
		t.Fatalf("wrong metric time, got '%d', want '%d'\n", m.Time, time.Unix())
	}

	if m.Mtype != mtype {
		t.Fatalf("wrong metric type, got '%s', want '%s'\n", m.Mtype, mtype)
	}
}
