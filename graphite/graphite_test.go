package graphite

import (
	"testing"
	"time"
)

func TestTimeFormat(t *testing.T) {
	now := time.Now().UTC()

	got := timeFormat(now)
	want := now.Unix()

	if got != want {
		t.Fatalf("got '%v', want '%v'\n", got, want)
	}
}
