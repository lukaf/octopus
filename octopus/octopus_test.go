package octopus

import (
	"testing"
	"time"
)

func TestTimeWindow(t *testing.T) {
	start, end := timeWindow()

	if end.Sub(start) != time.Hour*24 {
		t.Fatal("start and end are not exactly one day apart")
	}
}

func TestMakeUrl(t *testing.T) {
	got := makeUrl(
		"http://example.com/api/v1",
		map[string]string{
			"key": "value",
			"one": "1",
		},
	)

	want := "http://example.com/api/v1?key=value&one=1"

	if got != want {
		t.Fatalf("got '%s', want '%s'\n", got, want)
	}
}

func TestTimeToString(t *testing.T) {
	now := time.Now().UTC()

	want := now.Format(TimeFormat)
	got := TimeToString(now)

	if got != want {
		t.Fatalf("got '%s', want '%s'\n", got, want)
	}
}

func TestStringToTime(t *testing.T) {
	now := time.Now().UTC()
	s := now.Format(TimeFormat)

	want, err := time.Parse(TimeFormat, s)
	if err != nil {
		t.Fatalf("failed to parse time: %s", err)
	}

	got, err := StringToTime(s)
	if err != nil {
		t.Fatalf("error parsing string to time: %s\n", err)
	}

	if got != want {
		t.Fatalf("got '%s', want '%s'\n", got, want)
	}
}
