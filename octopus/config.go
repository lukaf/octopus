package octopus

import "os"

type Config struct {
	BaseEndpoint string
	ApiKey       string
}

func GetConfig() Config {
	return Config{
		BaseEndpoint: GetSecret("OCTOPUS_BASE_ENDPOINT"),
		ApiKey:       GetSecret("OCTOPUS_API_KEY"),
	}
}

func GetSecret(name string) string {
	return os.Getenv(name)
}
