package octopus

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func TestGetElectricityMetrics(t *testing.T) {
	apiKey := "myaipkey"
	authHeader := ""

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader = r.Header.Get("Authorization")
		fmt.Fprintln(
			w,
			`
{
    "count": 48,
    "next": null,
    "previous": null,
    "results": [
        {
            "consumption": 0.063,
            "interval_start": "2018-05-19T00:30:00+01:00",
            "interval_end": "2018-05-19T01:00:00+01:00"
        },
        {
            "consumption": 0.071,
            "interval_start": "2018-05-19T00:00:00+01:00",
            "interval_end": "2018-05-19T00:30:00+01:00"
        },
        {
            "consumption": 0.073,
            "interval_start": "2018-05-18T23:30:00+01:00",
            "interval_end": "2018-05-18T00:00:00+01:00"
        }
    ]
}`,
		)
	})

	ts := httptest.NewServer(handler)
	defer ts.Close()

	if err := os.Setenv("OCTOPUS_BASE_ENDPOINT", ts.URL); err != nil {
		t.Fatal("can't set OCTOPUS_BASE_ENDPOINT environment variable")
	}

	if err := os.Setenv("OCTOPUS_API_KEY", apiKey); err != nil {
		t.Fatal("can't set OCTOPUS_API_KEY environment variable")
	}

	results, err := GetElectricityMetrics()

	if err != nil {
		t.Fatalf("failed to create get metrics request: %s", err)
	}

	expectedAuthHeader := fmt.Sprintf(
		"Basic %s",
		base64.StdEncoding.EncodeToString([]byte(apiKey+":")),
	)

	if expectedAuthHeader != authHeader {
		t.Fatalf("expected auth header '%s', got '%s'\n", expectedAuthHeader, authHeader)
	}

	if len(results) != 3 {
		t.Fatal("unexpected number of results in get metrics response")
	}

	if _, err := StringToTime(results[0].IntervalEnd); err != nil {
		t.Fatalf("error parsing time: %s\n", err)
	}

	if _, err := StringToTime(results[0].IntervalStart); err != nil {
		t.Fatalf("error parsing time: %s\n", err)
	}
}

func TestGetGasMetrics(t *testing.T) {
	apiKey := "myaipkey"
	authHeader := ""

	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authHeader = r.Header.Get("Authorization")
		fmt.Fprintln(
			w,
			`
{
    "count": 48,
    "next": null,
    "previous": null,
    "results": [
        {
            "consumption": 0.063,
            "interval_start": "2018-05-19T00:30:00+01:00",
            "interval_end": "2018-05-19T01:00:00+01:00"
        },
        {
            "consumption": 0.071,
            "interval_start": "2018-05-19T00:00:00+01:00",
            "interval_end": "2018-05-19T00:30:00+01:00"
        },
        {
            "consumption": 0.073,
            "interval_start": "2018-05-18T23:30:00+01:00",
            "interval_end": "2018-05-18T00:00:00+01:00"
        }
    ]
}`,
		)
	})

	ts := httptest.NewServer(handler)
	defer ts.Close()

	if err := os.Setenv("OCTOPUS_BASE_ENDPOINT", ts.URL); err != nil {
		t.Fatal("can't set OCTOPUS_BASE_ENDPOINT environment variable")
	}

	if err := os.Setenv("OCTOPUS_API_KEY", apiKey); err != nil {
		t.Fatal("can't set OCTOPUS_API_KEY environment variable")
	}

	results, err := GetGasMetrics()

	if err != nil {
		t.Fatalf("failed to create get metrics request: %s", err)
	}

	expectedAuthHeader := fmt.Sprintf(
		"Basic %s",
		base64.StdEncoding.EncodeToString([]byte(apiKey+":")),
	)

	if expectedAuthHeader != authHeader {
		t.Fatalf("expected auth header '%s', got '%s'\n", expectedAuthHeader, authHeader)
	}

	if len(results) != 3 {
		t.Fatal("unexpected number of results in get metrics response")
	}

	if _, err := StringToTime(results[0].IntervalEnd); err != nil {
		t.Fatalf("error parsing time: %s\n", err)
	}

	if _, err := StringToTime(results[0].IntervalStart); err != nil {
		t.Fatalf("error parsing time: %s\n", err)
	}
}
