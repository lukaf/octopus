package octopus

import (
	"net/url"
	"time"
)

const TimeFormat = time.RFC3339

func timeWindow() (time.Time, time.Time) {
	now := time.Now().UTC()
	yesterday := now.Add(-time.Hour * 24)

	return time.Date(yesterday.Year(), yesterday.Month(), yesterday.Day(), 0, 0, 0, 0, time.UTC),
		time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC)

}

func makeUrl(base string, values map[string]string) string {
	v := url.Values{}

	for key, value := range values {
		v.Set(key, value)
	}

	return base + "?" + v.Encode()
}

func TimeToString(t time.Time) string {
	return t.Format(TimeFormat)
}

func StringToTime(s string) (time.Time, error) {
	return time.Parse(TimeFormat, s)
}
