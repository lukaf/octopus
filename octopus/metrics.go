package octopus

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type Consumption struct {
	Count   int                  `json:"count"`
	Next    string               `json:"next"`
	Results []ConsumptionResults `json:"results"`
}

type ConsumptionResults struct {
	Consumption   float64 `json:"consumption"`
	IntervalStart string  `json:"interval_start"`
	IntervalEnd   string  `json:"interval_end"`
}

func GetGasMetrics() ([]ConsumptionResults, error) {
	gasEndpoint := "gas-meter-points/1103997401/meters/E6S10029082161/consumption/"

	config := GetConfig()
	gasMetricsUrl, err := url.JoinPath(config.BaseEndpoint, gasEndpoint)
	if err != nil {
		return nil, fmt.Errorf("Error joining base URL and eletricity endpoint paths: %s", err)
	}

	start, end := timeWindow()

	finalUrl := makeUrl(
		gasMetricsUrl,
		map[string]string{
			"period_from": TimeToString(start),
			"period_to":   TimeToString(end),
		},
	)

	return getMetrics(finalUrl, config.ApiKey)
}

func GetElectricityMetrics() ([]ConsumptionResults, error) {
	electricityEndpoint := "electricity-meter-points/1800031888921/meters/21L4043704/consumption/"

	config := GetConfig()
	electricityMetricsUrl, err := url.JoinPath(config.BaseEndpoint, electricityEndpoint)
	if err != nil {
		return nil, fmt.Errorf("Error joining base URL and eletricity endpoint paths: %s", err)
	}

	start, end := timeWindow()

	finalUrl := makeUrl(
		electricityMetricsUrl,
		map[string]string{
			"period_from": TimeToString(start),
			"period_to":   TimeToString(end),
		},
	)

	return getMetrics(finalUrl, config.ApiKey)
}

func getMetrics(endpoint string, apiKey string) ([]ConsumptionResults, error) {
	oc := Consumption{
		Results: make([]ConsumptionResults, 0),
	}

	results := make([]ConsumptionResults, 0)

	client := &http.Client{}

	for {
		req, err := http.NewRequest("GET", endpoint, nil)
		if err != nil {
			return nil, fmt.Errorf("Error creating new request: %s", err)
		}
		req.SetBasicAuth(apiKey, "")

		response, err := client.Do(req)
		if err != nil {
			return nil, fmt.Errorf("error sending request: %s", err)
		}
		defer response.Body.Close()

		body, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return nil, fmt.Errorf("error reading response body: %s", err)
		}

		if err := json.Unmarshal(body, &oc); err != nil {
			return nil, fmt.Errorf("Error decoding response body: %s", err)
		}

		results = append(results, oc.Results...)

		if oc.Next == "" {
			break
		}

		endpoint = oc.Next
	}

	return results, nil
}
