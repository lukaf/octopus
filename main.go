package main

import (
	"fmt"
	"log"
	"octopus/graphite"
	"octopus/octopus"
	"os"
	"sort"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
)

func InLambdaEnv() bool {
	return os.Getenv("AWS_LAMBDA_FUNCTION_NAME") != ""
}

func run() error {
	g, err := octopus.GetGasMetrics()
	if err != nil {
		return err
	}

	e, err := octopus.GetElectricityMetrics()
	if err != nil {
		return err
	}

	gMetrics := graphite.NewMetrics(
		"testing.metric.gas",
		graphite.Gauge,
		time.Minute*30,
	)

	eMetrics := graphite.NewMetrics(
		"testing.metric.electricity",
		graphite.Gauge,
		time.Minute*30,
	)

	for _, i := range g {
		timestamp, err := octopus.StringToTime(i.IntervalStart)
		if err != nil {
			log.Println("Error converting timestamp:", err)
			continue
		}
		gMetrics.Add(i.Consumption, timestamp)
	}

	log.Println("Sending gas metrics")
	if err := graphite.Send(gMetrics); err != nil {
		return err
	}

	for _, i := range e {
		timestamp, err := octopus.StringToTime(i.IntervalStart)
		if err != nil {
			fmt.Println("Error converting timestamp:", err)
			continue
		}
		eMetrics.Add(i.Consumption, timestamp)
	}

	log.Println("Sending electricity metrics")
	if err := graphite.Send(eMetrics); err != nil {
		return err
	}

	return nil
}

func debug() bool {
	return false
}

func main() {
	if debug() {
		test()
		os.Exit(0)
	}

	if InLambdaEnv() {
		lambda.Start(run)
	} else {
		fmt.Println(run())
	}
}

func test() error {
	now := time.Now().UTC()

	metricName := "testing.metrics.one"
	interval := time.Minute * 30
	metricType := graphite.Gauge

	m := graphite.NewMetrics(metricName, metricType, interval)
	for _, i := range []int{1, 2, 3, 4} {
		m.Add(float64(i), now.Add(-time.Minute*time.Duration(30*i)))
	}

	fmt.Println(m)
	sort.Sort(m)
	fmt.Println(m)
	return nil
}
