resource "aws_iam_user" "octopus" {
  name = "octopus"
}

resource "aws_iam_user_policy" "octopus" {
  name   = "octopus"
  user   = aws_iam_user.octopus.name
  policy = data.aws_iam_policy_document.octopus_user.json
}

data "aws_iam_policy_document" "octopus_user" {
  statement {
    effect    = "Allow"
    actions   = ["s3:PutObject", "s3:PutObjectAcl"]
    resources = ["${aws_s3_bucket.octopus.arn}/*"]
  }
}
