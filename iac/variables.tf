variable "iam_role" {
  type = string
  validation {
    condition     = can(regex("^arn:aws:iam::[0-9]+:role/*", var.iam_role))
    error_message = "Invalid iam_role value"
  }
}

variable "octopus_base_endpoint" {
  type = string
}

variable "octopus_api_key" {
  type = string
}

variable "graphite_username" {
  type = string
}

variable "graphite_password" {
  type = string
}

variable "graphite_endpoint" {
  type = string
}
