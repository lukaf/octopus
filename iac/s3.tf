resource "aws_s3_bucket" "octopus" {
  bucket = "evriala-octopus"

  tags = {
    Name = "octopus"
  }
}

resource "aws_s3_bucket_public_access_block" "octopus" {
  bucket = aws_s3_bucket.octopus.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

data "aws_s3_objects" "octopus_releases" {
  bucket = aws_s3_bucket.octopus.id
  prefix = "main/"
}
