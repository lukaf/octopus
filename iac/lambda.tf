locals {
  function_name                  = "octopus"
  release_number_strings         = [for release in data.aws_s3_objects.octopus_releases.keys : element(split("/", release), 1)]
  release_numbers_padded_ordered = sort(formatlist("%03d", local.release_number_strings))
  latest_release_number = tonumber(
    element(
      local.release_numbers_padded_ordered,
      length(local.release_numbers_padded_ordered) - 1
    )
  )
  latest_release_archive = one(
    [for release in data.aws_s3_objects.octopus_releases.keys : release if startswith(
      release,
      format("main/%d/", local.latest_release_number)
      )
    ]
  )
}

resource "aws_lambda_function" "octopus" {
  function_name = local.function_name
  handler       = "octopus"

  role = aws_iam_role.lambda.arn

  runtime = "go1.x"

  s3_bucket = aws_s3_bucket.octopus.id
  s3_key    = local.latest_release_archive

  environment {
    variables = {
      "OCTOPUS_BASE_ENDPOINT" = var.octopus_base_endpoint
      "OCTOPUS_API_KEY"       = var.octopus_api_key
      "GRAPHITE_USERNAME"     = var.graphite_username
      "GRAPHITE_PASSWORD"     = var.graphite_password
      "GRAPHITE_ENDPOINT"     = var.graphite_endpoint
    }
  }
}

resource "aws_cloudwatch_log_group" "octopus" {
  name              = "/aws/lambda/${local.function_name}"
  retention_in_days = 7
}

resource "aws_cloudwatch_event_rule" "octopus" {
  name                = "octopus"
  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "octopus" {
  arn  = aws_lambda_function.octopus.arn
  rule = aws_cloudwatch_event_rule.octopus.name
}

resource "aws_lambda_permission" "cron" {
  statement_id  = "AllowEventSchedule"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.octopus.function_name
  principal     = "events.amazonaws.com"
}
